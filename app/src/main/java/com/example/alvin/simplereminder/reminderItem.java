package com.example.alvin.simplereminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class reminderItem {
    public AlarmManager alarmForReminder;
    public PendingIntent alarmIntent;
    private String name;
    private Time time;
    private Date date;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getTime(){
        String timeToReturn=time.toString();
        return timeToReturn.substring(0,timeToReturn.length()-3);
    }

    public void setTime(String time){
        this.time=java.sql.Time.valueOf(time);
    }

    public String getDate(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String text=df.format(date);
        return text;
    }

    public void setDate(String date) throws ParseException {
        SimpleDateFormat fm1=new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date dateTwo=fm1.parse(date);
        java.sql.Date sqlStartDate = new java.sql.Date(dateTwo.getTime());
        this.date=sqlStartDate;
    }
}
