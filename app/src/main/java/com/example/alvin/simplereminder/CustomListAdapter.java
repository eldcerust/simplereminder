package com.example.alvin.simplereminder;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CustomListAdapter extends BaseAdapter  implements MainActivity.onReceiveRedPosition{
    private ArrayList<reminderItem> listData;
    private LayoutInflater layoutInflater;
    private ArrayList<Integer> positionToRed;

    public CustomListAdapter(Context aContext, ArrayList<reminderItem> listData){
        this.listData=listData;
        layoutInflater=LayoutInflater.from(aContext);
    }

    @Override
    public int getCount(){
        return listData.size();
    }

    @Override
    public Object getItem(int position){
        return listData.get(position);
    }

    public long getItemId(int position){
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder;
        if (convertView == null){
            convertView=layoutInflater.inflate(R.layout.list_row_layout,null);
            holder=new ViewHolder();
            holder.nameView=(TextView)convertView.findViewById(R.id.title);
            holder.timeView=(TextView)convertView.findViewById(R.id.time);
            holder.dateView=(TextView)convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        } else {
            holder=(ViewHolder)convertView.getTag();
        }
        holder.nameView.setText(listData.get(position).getName());
        holder.timeView.setText(listData.get(position).getTime());
        holder.dateView.setText(listData.get(position).getDate());

        try{
            if(this.positionToRed!=null && this.positionToRed.contains(position)){
                Log.i("Got executed","Code red");
                holder.nameView.setTextColor(Color.RED);
                holder.timeView.setTextColor(Color.RED);
                holder.dateView.setTextColor(Color.RED);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public void onReceiveRedPosition(ArrayList<Integer> positionToRed){
        this.positionToRed=positionToRed;
    }

    static class ViewHolder{
        TextView nameView;
        TextView timeView;
        TextView dateView;
    }

}
