package com.example.alvin.simplereminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    public AlarmManager alarmMgr;
    public ArrayList<PendingIntent> intentArray;
    private static final int NOTIFICATION_ID=101;
    private static final String TAG = "YourNotification";
    public ArrayList<reminderItem> reminder_details;
    public ListView lvl;
    public static reminderItem editing;
    public ArrayList<Integer> positionToRed;
    onReceiveRedPosition sendToCustomlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvl = (ListView) findViewById(R.id.listView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,"default")
                .setSmallIcon(R.raw.aperture)
                .setContentTitle("Simple Reminder Is Running.")
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setOngoing(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(TAG,NOTIFICATION_ID,mBuilder.build());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent=new Intent(MainActivity.this,newReminders.class);
                myIntent.putExtra("EDITMODE",Integer.MAX_VALUE);
                startActivityForResult(myIntent,2);
            }
        });

        setListView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2){
            Log.i("Request code","received");
            if(resultCode==RESULT_OK){
                setListView();
            }
        }
    }

    public Boolean compareDates(Calendar thisOne, Calendar thatOne){
        boolean validity;
        Log.i("Current data/reminderDate",thisOne.toString()+" "+thatOne.toString());
        if(thisOne.get(Calendar.MINUTE)>=thatOne.get(Calendar.MINUTE) && thisOne.get(Calendar.YEAR)>=thatOne.get(Calendar.YEAR) && thisOne.get(Calendar.MONTH)>=thatOne.get(Calendar.MONTH) && thisOne.get(Calendar.DAY_OF_MONTH)>=thatOne.get(Calendar.DAY_OF_MONTH) && thisOne.get(Calendar.HOUR_OF_DAY)>=thatOne.get(Calendar.HOUR_OF_DAY)){
            Log.i("Date is","smaller or equal");
            validity=true;
        } else {
            validity=false;
        }

        return validity;
    }

    public void setListView(){
        /*test unit*/
        try {
            reminder_details = getListData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        CustomListAdapter listAdapter=new CustomListAdapter(this, reminder_details);
        try {
            Log.i("Position to red in customlist",positionToRed.toString());
            listAdapter.onReceiveRedPosition(positionToRed);
        } catch(Exception e){
            e.printStackTrace();
        }
        lvl.setAdapter(listAdapter);
        lvl.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = lvl.getItemAtPosition(position);
                editing=(reminderItem) o;
                Intent myIntent=new Intent(MainActivity.this,newReminders.class);
                myIntent.putExtra("EDITMODE",position);
                startActivityForResult(myIntent,2);

            }
        });
    }

    private ArrayList getListData() throws ParseException {
        ArrayList<reminderItem> results = new ArrayList<reminderItem>();
        reminderItem reminderData = new reminderItem();
        positionToRed=new ArrayList<Integer>();

        try{
            SQLiteDatabase myDatabase = this.openOrCreateDatabase("Reminders",MODE_PRIVATE,null);
            Cursor c = myDatabase.rawQuery("SELECT * FROM reminders ORDER BY year, month, day, hour, minute", null);

            Calendar alarmForReminder = Calendar.getInstance();
            alarmMgr=(AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            intentArray=new ArrayList<PendingIntent>();
            int count=0;

            int titleIndex=c.getColumnIndex("title");
            int hourIndex=c.getColumnIndex("hour");
            int minuteIndex=c.getColumnIndex("minute");
            int yearIndex=c.getColumnIndex("year");
            int monthIndex=c.getColumnIndex("month");
            int dayIndex=c.getColumnIndex("day");

            c.moveToFirst();
            while(c!=null){
                Intent intent = new Intent(this,AlarmReceiver.class);

                Calendar currentDateTime=Calendar.getInstance();

                reminderData.setName(c.getString(titleIndex));
                reminderData.setDate(c.getString(dayIndex)+"/"+c.getString(monthIndex)+"/"+c.getString(yearIndex));

                alarmForReminder.set(Calendar.DAY_OF_MONTH,Integer.valueOf(c.getString(dayIndex)));
                alarmForReminder.set(Calendar.MONTH,Integer.valueOf(c.getString(monthIndex)));
                alarmForReminder.set(Calendar.YEAR,Integer.valueOf(c.getString(yearIndex)));

                reminderData.setTime(c.getString(hourIndex)+":"+c.getString(minuteIndex)+":00");

                alarmForReminder.set(Calendar.HOUR_OF_DAY,Integer.valueOf(c.getString(hourIndex)));
                alarmForReminder.set(Calendar.MINUTE,Integer.valueOf(c.getString(minuteIndex)));

                intent.putExtra("YAES",reminderData.getName()+","+reminderData.getTime());
                PendingIntent pendingIntent=PendingIntent.getBroadcast(this,count,intent,PendingIntent.FLAG_UPDATE_CURRENT);

                results.add(reminderData);
                alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,alarmForReminder.getTimeInMillis(),pendingIntent);

                intentArray.add(pendingIntent);
                Log.i("Intent","Added on count"+String.valueOf(count));
                addPositionToRed(count,currentDateTime,alarmForReminder);

                count+=1;

                reminderData=new reminderItem();
                c.moveToNext();
            }
            System.gc();
            return results;
        } catch (Exception e) {
            return results;
        }}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public interface onReceiveRedPosition{
        public void onReceiveRedPosition(ArrayList<Integer> positionToRed);
    }

    public void addPositionToRed(int position,Calendar movingOne, Calendar staticOne){
        if(compareDates(movingOne,staticOne) && !positionToRed.contains(position)){
            positionToRed.add(position);
            Log.i("Position to red now",positionToRed.toString());
            System.out.println(position);
            Log.i("Match found","at time count"+String.valueOf(position));
        }
    }
}
