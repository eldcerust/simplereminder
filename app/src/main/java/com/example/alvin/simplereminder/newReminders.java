package com.example.alvin.simplereminder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Delayed;

public class newReminders extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener,ListDialogFragment.OnDialogDismissListener {
    public String dayPicked, monthPicked, yearPicked,hourPicked,minutePicked;
    public String prevTitle,prevDate,prevTime;
    public TextView dateDisplay, timeDisplay;
    public TextInputLayout titleInserted;
    public int editMode;
    public reminderItem reminderData;
    public Button snoozeButton, createButton, doneButton;
    public Integer[] timeAdded={1,3,5,10,15,30,60};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reminders);

        titleInserted=findViewById(R.id.textInputLayout);
        dateDisplay=(TextView)findViewById(R.id.dateDisplay);
        timeDisplay=(TextView)findViewById(R.id.timeDisplay);
        Calendar cal = Calendar.getInstance();/*test unit*/
        editMode=getIntent().getIntExtra("EDITMODE",-1);

        createButton=(Button)findViewById(R.id.createButton);
        snoozeButton=(Button)findViewById(R.id.snoozeButton);
        doneButton=(Button)findViewById(R.id.actuallyDoneButton);

        if(editMode==Integer.MAX_VALUE){
            snoozeButton.setVisibility(View.INVISIBLE);
            doneButton.setVisibility(View.INVISIBLE);
            createButton.setText("Create");

            Log.i("Editing attempt","False");
            dayPicked=returnStringForZeros(cal.get(Calendar.DAY_OF_MONTH));
            monthPicked=returnStringForZeros(cal.get(Calendar.MONTH));
            yearPicked=returnStringForZeros(cal.get(Calendar.YEAR));

            hourPicked=returnStringForZeros(cal.get(Calendar.HOUR_OF_DAY));
            minutePicked=returnStringForZeros(cal.get(Calendar.MINUTE));
        }else{
            createButton.setText("Change");

            Log.i("Editing attempt","True");
            reminderData=MainActivity.editing;
            dayPicked=reminderData.getDate().substring(0,2);
            monthPicked=reminderData.getDate().substring(3,5);
            yearPicked=reminderData.getDate().substring(6,10);

            prevDate=reminderData.getDate();

            hourPicked=reminderData.getTime().substring(0,2);
            minutePicked=reminderData.getTime().substring(3,5);

            prevTime=reminderData.getTime();
            prevTitle=reminderData.getName();
            titleInserted.getEditText().setText(prevTitle);
        }

        dateDisplay.setText(dayPicked+"/"+monthPicked+"/"+yearPicked);
        timeDisplay.setText(hourPicked+":"+minutePicked);
    }

    public void showDatePicker(View v) {
        DialogFragment newFragment = new MyDatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "date picker");
    }

    public String returnStringForZeros(Integer valueWanted){
        if(valueWanted<10){
            return("0"+String.valueOf(valueWanted));
        } else {
            return String.valueOf(valueWanted);
        }
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if(view.getDayOfMonth()<10){
            dayPicked="0"+String.valueOf(view.getDayOfMonth());
        } else {
            dayPicked=String.valueOf(view.getDayOfMonth());
        }

        if(view.getMonth()<10){
            monthPicked="0"+String.valueOf(view.getMonth());
        } else {
            monthPicked=String.valueOf(view.getMonth());
        }

        yearPicked=String.valueOf(view.getYear());
        dateDisplay.setText(dayPicked+"/"+monthPicked+"/"+yearPicked);
    }

    public void showTimePicker(View v){
        DialogFragment newFragment = new MyTimePickerFragment();
        newFragment.show(getSupportFragmentManager(),"time picker");
    }

    public void onTimeSet(TimePicker view, int hour, int minutes){
        if(view.getHour()<10){
            hourPicked="0"+String.valueOf(view.getHour());
        } else {
            hourPicked=String.valueOf(view.getHour());
        }

        if(view.getMinute()<10){
            minutePicked="0"+String.valueOf(view.getMinute());
        } else {
            minutePicked=String.valueOf(view.getMinute());
        }
        timeDisplay.setText(hourPicked+":"+minutePicked);
    }

    public void createButton(View v){
        //insert code here for taking out text from textinputlayout
        String titleString=titleInserted.getEditText().getText().toString();
        if(editMode!=Integer.MAX_VALUE) {
            try{
                String[] prevDateParts=prevDate.split("/");
                String[] prevTimeParts=prevTime.split(":");
                Log.i("titleString","DELETE FROM reminders WHERE title = \'"+prevTitle+"\' AND date = \'"+prevDate+"\' AND time= \'"+prevTime+":00\'");
                SQLiteDatabase myDatabase = this.openOrCreateDatabase("Reminders", MODE_PRIVATE, null);/*test unit*/
                myDatabase.execSQL("DELETE FROM reminders WHERE title = \'"+prevTitle+"\' AND year = "+prevDateParts[2]+" AND month = "+prevDateParts[1]+" AND day = "+prevDateParts[0]+" AND hour = "+prevTimeParts[0]+" AND minute = "+prevTimeParts[1]);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        try {
            SQLiteDatabase myDatabase = this.openOrCreateDatabase("Reminders", MODE_PRIVATE, null);
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS reminders (title VARCHAR, hour INTEGER, minute INTEGER ,year INTEGER, month INTEGER, day INTEGER)");
            myDatabase.execSQL("INSERT INTO reminders(title, hour, minute ,year ,month ,day) values (\'" + titleString + "\', " + hourPicked + ", " + minutePicked + ", "+yearPicked+", "+monthPicked+", "+dayPicked+")");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent resultIntent= new Intent();
        resultIntent.putExtra("some_key","String_data");
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    public void doneButton(View v) {
        try {
            String[] prevDateParts = prevDate.split("/");
            String[] prevTimeParts = prevTime.split(":");
            Log.i("titleString", "DELETE FROM reminders WHERE title = \'" + prevTitle + "\' AND date = \'" + prevDate + "\' AND time= \'" + prevTime + ":00\'");
            SQLiteDatabase myDatabase = this.openOrCreateDatabase("Reminders", MODE_PRIVATE, null);
            myDatabase.execSQL("DELETE FROM reminders WHERE title = \'" + prevTitle + "\' AND year = " + prevDateParts[2] + " AND month = " + prevDateParts[1] + " AND day = " + prevDateParts[0] + " AND hour = " + prevTimeParts[0] + " AND minute = " + prevTimeParts[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent resultIntent= new Intent();
        resultIntent.putExtra("some_key","String_data");
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    public void snoozeButton(View v){
        DialogFragment something=new ListDialogFragment();
        something.show(getSupportFragmentManager(),"List thing");
    }

    public void onDialogDismissListener(int position){
        String titleString=titleInserted.getEditText().getText().toString();/*tested unit*/
        try{
            String[] prevDateParts = prevDate.split("/");
            String[] prevTimeParts = prevTime.split(":");
            Calendar currentTime=Calendar.getInstance();
            currentTime.add(Calendar.MINUTE,timeAdded[position]);
            String[] dateParts={String.valueOf(currentTime.get(Calendar.DAY_OF_MONTH)),String.valueOf(currentTime.get(Calendar.MONTH)),String.valueOf(currentTime.get(Calendar.YEAR))};
            String[] hourParts={String.valueOf(currentTime.get(Calendar.HOUR_OF_DAY)),String.valueOf(currentTime.get(Calendar.MINUTE))};
            SQLiteDatabase myDatabase = this.openOrCreateDatabase("Reminders", MODE_PRIVATE, null);
            myDatabase.execSQL("DELETE FROM reminders WHERE title = \'" + prevTitle + "\' AND year = " + prevDateParts[2] + " AND month = " + prevDateParts[1] + " AND day = " + prevDateParts[0] + " AND hour = " + prevTimeParts[0] + " AND minute = " + prevTimeParts[1]);
            myDatabase.execSQL("INSERT INTO reminders(title, hour, minute ,year ,month ,day) values (\'" + titleString + "\', " +hourParts[0]+ ", " +hourParts[1]+ ", "+dateParts[2]+", "+dateParts[1]+", "+dateParts[0]+")");
        } catch (Exception e){
            e.printStackTrace();
        }
        Intent resultIntent= new Intent();
        resultIntent.putExtra("some_key","String_data");
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
